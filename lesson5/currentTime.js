const ONE_MINUTE = 60000;
const PAD_TIME_STRING = "0";

function currentTime() {
  let date = new Date();
  let hours = date.getHours()
    .toString()
    .padStart(2, PAD_TIME_STRING);
  let minutes = date.getMinutes()
    .toString()
    .padStart(2, PAD_TIME_STRING);
  let time = `${hours}:${minutes}`;
  console.log(time);
}

setInterval(currentTime, ONE_MINUTE);

currentTime();
