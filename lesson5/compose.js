function compose(...argsFunctions) {
  function callFunctions(...argsParameters) {
    for (let index = argsFunctions.length - 1; index >= 0 ; index--) {
      argsParameters = [argsFunctions[index](...argsParameters)];
    }

    return argsParameters[0];
  }

  return callFunctions;
}

function plusOne(x) {
  return x + 1;
}

function negative(x) {
  return -x;
}

let f = compose(plusOne, negative, Math.pow);

console.log(f(3,4));
