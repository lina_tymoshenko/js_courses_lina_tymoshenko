function printFullName(firstName, middleName, lastName) {
  console.log(`${firstName} ${middleName} ${lastName}`);
}

function curryN(parametersAmount, curryFunction) {
  return function fn() {
    if (arguments.length < parametersAmount) {
      return fn.bind(null, ...arguments);
    } else {
      return curryFunction(...arguments);
    }
  }
}

const curriedPrintFullName = curryN(3, printFullName);

curriedPrintFullName("Harry")("James")("Potter");
