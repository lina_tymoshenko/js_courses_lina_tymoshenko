class SimpleClass {
  doSomething() {}
  saySomething() {}
}

class SecondClass {
  createSomething() {}
}

class ThirdClass extends SecondClass {
  changeSomething() {}
}

function showClassMethods(event) {
  let target = event.target;

  if (target.tagName != "LI") return;
  let className = target.innerText;

  showSelectedClassMethods(className, "SimpleClass", SimpleClass);
  showSelectedClassMethods(className, "SecondClass", SecondClass);
  showSelectedClassMethods(className, "ThirdClass", ThirdClass);
}

function showSelectedClassMethods(className, equalClassName, classLink) {
  if (className == equalClassName) {
    console.log(`${className}: `, Object.getOwnPropertyNames(Object.getPrototypeOf(new classLink())).join(', '))
  }
}

function showAllClassMethods() {
  console.log("SimpleClass: ", getAllFunctions(new SimpleClass()));
  console.log("SecondClass: ", getAllFunctions(new SecondClass()));
  console.log("ThirdClass: ", getAllFunctions(new ThirdClass()));
}

function getAllFunctions(obj) {
  let copyObj = obj;
  let props = [];

  do {
    props = props.concat(Object.getOwnPropertyNames(obj));
  } while (obj = Object.getPrototypeOf(obj));

  return props.sort()
    .filter((element, index, arr) => (element != arr[index + 1] && typeof copyObj[element] == 'function'))
    .join(', ');
}