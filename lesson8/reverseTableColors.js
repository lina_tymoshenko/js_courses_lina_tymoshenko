let rows = getRandomInt(5, 20);
let columns = getRandomInt(5, 20);
let table = document.getElementById("table");
let button = document.getElementById("button");
const WHITE_CELL_COLOR = "white";
const BLACK_CELL_COLOR = "black";
const TABLE_CELL = "td";
const TABLE_ROW = "tr";

buildTable();

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

button.onclick = function () {
  for (let rowIndex = 0; rowIndex < table.rows.length; rowIndex++) {
    let row = table.rows[rowIndex];

    for (let cellIndex = 0; cellIndex < row.cells.length; cellIndex++) {
      let column = row.cells[cellIndex];
      column.style.background = column.style.background === WHITE_CELL_COLOR ? BLACK_CELL_COLOR : WHITE_CELL_COLOR;
    }
  }
};

function buildTable() {
  for (let rowIndex = 1; rowIndex <= rows; rowIndex++) {
    let row = document.createElement(TABLE_ROW);

    for (let columnIndex = 1; columnIndex <= columns; columnIndex++) {
      let cell = document.createElement(TABLE_CELL);

      cell.style.background = WHITE_CELL_COLOR;

      if (rowIndex % 2) {
        if (columnIndex % 2 !== 1) {
          cell.style.background = BLACK_CELL_COLOR;
        }
      } else {
        if (columnIndex % 2) {
          cell.style.background = BLACK_CELL_COLOR;
        }
      }

      row.appendChild(cell);
    }

    table.appendChild(row);
  }
}
