class MyString {
  static reverse(string) {
    return string.split("")
      .reverse()
      .join("");
  }

  static ucFirst(string) {
    return string.charAt(0)
      .toUpperCase() + string.slice(1);
  }

  static ucWords(string) {
    return string.split(" ")
      .map(this.ucFirst)
      .join(" ");
  }
}

console.log(MyString.reverse('abcde'));
console.log(MyString.ucFirst('abcde'));
console.log(MyString.ucWords('abcde abcde abcde'));
