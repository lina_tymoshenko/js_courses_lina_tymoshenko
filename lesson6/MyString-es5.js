function MyString() {}

MyString.reverse = function(string) {
  var result = "";

  for (var index = string.length - 1; index >= 0; index--) {
    result += string[index];
  }

  return result;
};

MyString.ucFirst = function(string) {
  return string.charAt(0)
    .toUpperCase() + string.slice(1);
};

MyString.ucWords = function(string) {
   var result = string.split(" ");

   for (var index = 0; index < result.length; index++) {
     result[index] = this.ucFirst(result[index]);
   }

   return result.join(" ");
};

console.log(MyString.reverse('abcde'));
console.log(MyString.ucFirst('abcde'));
console.log(MyString.ucWords('abcde abcde abcde'));
