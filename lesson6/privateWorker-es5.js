function Worker(name, surname, rate, days) {
  var _name = name;
  var _surname = surname;
  var _rate = rate;
  var _days = days;

  this.getSalary = function() {
    return _rate * _days;
  };

  this.getName = function() {
    return _name;
  };

  this.getSurname = function() {
    return _surname;
  };

  this.getRate = function() {
    return _rate;
  };

  this.getDays = function() {
    return _days;
  };
}

var worker = new Worker('Иван', 'Иванов', 10, 31);

console.log(worker.getName());
console.log(worker.getSurname());
console.log(worker.getRate());
console.log(worker.getDays());
console.log(worker.getSalary());
