class Worker {
  constructor(name, surname, rate, days) {
    this._name = name;
    this._surname = surname;
    this._rate = rate;
    this._days = days;
  }

  getSalary() {
    return this._rate * this._days;
  }

  getName() {
    return this._name;
  }

  getSurname() {
    return this._surname;
  }

  getRate() {
    return this._rate;
  }

  getDays() {
    return this._days;
  }

  setRate(rate) {
    this._rate = rate;
  }

  setDays(days) {
    this._days = days;
  }
}

const worker = new Worker('Иван', 'Иванов', 10, 31);
worker.setRate(20);
worker.setDays(10);

console.log(worker.getName());
console.log(worker.getSurname());
console.log(worker.getRate());
console.log(worker.getDays());
console.log(worker.getSalary());