function createNode(name, klasses) {
  const node = document.createElement(name);
  node.className = klasses;
  return node;
}

function log(value) {
  console.log(value);
}

function addClass(node, ...klass) {
  const elementClasses = node.classList;
  elementClasses.add(...klass);
}

const addClassNode = createNode('div', 'test');
addClass(addClassNode, 'test1', 'test2');
log(addClassNode);
