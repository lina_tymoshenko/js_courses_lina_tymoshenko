function createNode(name, klasses) {
  const node = document.createElement(name);
  node.className = klasses;
  return node;
}

function log(value) {
  console.log(value);
}

function removeClass(node, ...klass) {
  const elementClasses = node.classList;
  elementClasses.remove(...klass);
}

const removeClassNode = createNode('div', 'test');
removeClass(removeClassNode, 'test', 'test1');
log(removeClassNode);
