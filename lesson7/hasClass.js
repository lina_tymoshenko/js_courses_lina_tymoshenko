function createNode(name, klasses) {
  const node = document.createElement(name);
  node.className = klasses;
  return node;
}

function log(value) {
  console.log(value);
}

function hasClass(node, klass) {
  const elementClasses = node.classList;
  return elementClasses.contains(klass);
}

const hasClassNode = createNode('div', 'test');
const hasClass1 = hasClass(hasClassNode, 'test');
log(hasClass1);
const hasClass2 = hasClass(hasClassNode, 'tes');
log(hasClass2);
