function httpGet(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = () => {
      if (xhr.status === 200) {
        let result = JSON.parse(xhr.response);
        resolve(result);
      } else {
        let error = new Error(xhr.statusText);
        error.code = xhr.status;
        reject(error);
      }
    };

    xhr.onerror = () => {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });

}

async function getData() {
  let urls = [
    "http://www.json-generator.com/api/json/get/cevhxOsZnS",
    "http://www.json-generator.com/api/json/get/cguaPsRxAi",
    "http://www.json-generator.com/api/json/get/cfDZdmxnDm",
    "http://www.json-generator.com/api/json/get/cfkrfOjrfS",
    "http://www.json-generator.com/api/json/get/ceQMMKpidK"
  ];

  const results = [];

  for (let index = 0; index < urls.length; index++) {
    let url = urls[index];
    results.push(await httpGet(url));
  }

  console.log(results);
}

getData();
