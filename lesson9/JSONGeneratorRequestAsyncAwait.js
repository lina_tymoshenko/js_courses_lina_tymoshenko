function httpGet(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = () => {
      if (xhr.status === 200) {
        let result = JSON.parse(xhr.response);
        resolve(result);
      } else {
        let error = new Error(xhr.statusText);
        error.code = xhr.status;
        reject(error);
      }
    };

    xhr.onerror = () => {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });

}

async function retrieveUsersData() {
  const USERS_DATA_URL_ACCESS = "http://www.json-generator.com/api/json/get/cfQCylRjuG";
  const USERS_DATA_URL = "http://www.json-generator.com/api/json/get/cfVGucaXPC";
  const {getUsersData} = await httpGet(USERS_DATA_URL_ACCESS);

  if (getUsersData) {
    const usersData = await httpGet(USERS_DATA_URL);
    console.log(usersData);
  }
}

retrieveUsersData();
