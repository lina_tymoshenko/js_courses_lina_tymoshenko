function httpGet(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = () => {
      if (xhr.status === 200) {
        let result = JSON.parse(xhr.response);
        resolve(result);
      } else {
        let error = new Error(xhr.statusText);
        error.code = xhr.status;
        reject(error);
      }
    };

    xhr.onerror = () => {
      reject(new Error("Network Error"));
    };

    xhr.send();
  });

}

function retrieveUsersData() {
  const USERS_DATA_URL_ACCESS = "http://www.json-generator.com/api/json/get/cfQCylRjuG";
  const USERS_DATA_URL = "http://www.json-generator.com/api/json/get/cfVGucaXPC";

  httpGet(USERS_DATA_URL_ACCESS)
    .then(({getUsersData}) => {
      if (getUsersData) {
        return httpGet(USERS_DATA_URL);
      }
    })
    .then(response => {
      if (response) {
        console.log(response);
      }
    })
}

retrieveUsersData();
